import {Logger, YinzOptions, Exception} from "@yinz/commons"
import Container from "@yinz/container/ts/Container";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import * as path from "path";
import * as yaml from "js-yaml";
import * as fs from "fs";
import * as _ from "lodash";
import { TypeormDao } from "@yinz/commons.data";

export interface DataloaderOptions {
    logger: Logger;
    container: Container;
}

export interface LoadResult {
    recs: {
        total: number;
        skipped: number;
        loaded: number;
    }    
}

export type DataLoaderFunctionsOptions = YinzOptions & {
    cwd ?: string 
    defaultLang?: string;
    locales?: string[];
}

export default class Dataloader {

    private _logger: Logger;
    private _container: Container;
    private _cache: any;

    constructor( options: DataloaderOptions) {

        this._logger = options.logger;
        this._container = options.container;
        this._cache = {};
    }
 
    
    public async load(handler: YinzConnection | YinzTransaction, filenames: string[] | string, options?: DataLoaderFunctionsOptions ): Promise<any> {

        this._logger.trace(`Enter Dataloader.load(handler %s, filenames %j, options %s)`, handler.id, filenames, options);

        this._logger.info(`About to load files %j...`, filenames);
        
        let result: any = {}
        options = options || {};

        filenames = Array.isArray(filenames) ? filenames : [filenames];

        // for each file

        for ( let filename of filenames) {

            this._logger.info(`About to load file %s...`, filename);

            filename = options.cwd ? options.cwd + "/" + filename : filename;

            // parse filename
            let file = path.parse(filename);

            // load file content
            let sets = yaml.safeLoad(fs.readFileSync(filename, 'utf8')) as any;

            // load sets to database            
            for ( let setName of Object.keys(sets) ) {

                this._logger.info('    About to load set %s... ', setName);

                this._logger.info('    File %s containes %d set(s)...', filename, Object.keys(sets).length);


                // Incase setName equal `$files`, it means it is reffering other file, thus we will call load recursively.
                // We also have to make sure to change the `cwd` relative to the file dir 
                if ( setName === "$files" ) {

                    let tempResult = await this.load(handler, sets[setName], {...options, cwd: file.dir});
                    this.updateResult(result, tempResult);

                } else {
                    // incase the setName is not `$files`, it means it is a database entity set
                    await this.loadDataSet(handler, setName, sets[setName], result, options);
                }

            }                        
        }

        
        // this._container;
        // this._cache;

        this._logger.trace('Leave DataLoader.load() -> %j', result)

        return result;

    }

    public resetCache(options?) {
        this._logger.trace('Enter DataLoader.resetCache(options: %s)', '...', '...');


        this._cache = {};


        this._logger.trace('Leave DataLoader.resetCache() -> void');
    }
    
    private updateResult(all_steps_results: any, last_setup_result: any ): void {
        
        for (let prop in last_setup_result) {

            if ( all_steps_results.hasOwnProperty(prop)) {
                all_steps_results[prop].recs.total += last_setup_result[prop].recs.total;
                all_steps_results[prop].recs.loaded += last_setup_result[prop].recs.loaded;
                all_steps_results[prop].recs.skipped += last_setup_result[prop].recs.skipped;
            } else {
                all_steps_results[prop] = last_setup_result[prop];
            }
        }

    }

    private async loadDataSet(handler: YinzConnection | YinzTransaction, setName: string, set: any, result: any, options?: DataLoaderFunctionsOptions ): Promise<void> {

        this._logger.trace(`Enter Dataloader.loadDataSet(handler %s, setName %s,  set %j, options %s)`, handler.id, setName, set, options);

        this._logger.info('    About to load set %s...', setName);

        let keys = set.$keys;
        let recs = set.$recs || set;

        result[setName] = {
            recs: {
                total: recs.length,
                loaded: 0,
                skipped: 0,
            }
        }

        // load records to database
        
        // recs.forEach( async (rec, recIdx) => { 

        for ( let recIdx = 0; recIdx < recs.length; recIdx++) {

            let rec = recs[recIdx];
        
            this._logger.info('      About to load rec %d...', recIdx);

            this._logger.debug('        the rec being loaded %j...', rec);

            // variable that will hold i18n for the current record
            let i18nData = {};

            // create record 
            let instance = await this.createOrSaveRec(handler, setName, keys, rec, i18nData, options);

            this._logger.debug('        the rec has been loaded %j...', rec);

            result[setName].recs.loaded++;

            this._logger.debug('      About to load i18n data of rec %d...', recIdx);
            await this.loadI18nData(handler, setName, instance, i18nData, options);

        }

        this._logger.trace('Leave DataLoader.__loadDataSet__() -> void');


    }

    private async loadI18nData(handler: YinzConnection | YinzTransaction, setName: string, instance: any, i18nData: any, options?: DataLoaderFunctionsOptions): Promise<any> {

        this._logger.trace(`Enter Dataloader.loadI18nData(handler %s, setName %s,  instance %j, i18nData %j, options %j)`, handler.id, setName, instance, i18nData, options);

        options = options || {};
        
        let locales = options.locales || ['ar', 'fr', 'en', 'dk']
        let uniqueKeys: any = { ownerId: instance.id };
        let createOrSaveOptions = {...options, uniqueKeys }

        if ( !_.isEmpty(i18nData) ) {

            for ( let locale of locales ) {

                let i18nRec: any = {};

                // load i18n fileds
                for ( let colName of Object.keys(i18nData) ) {
                    this._logger.debug('            Found....', locale,  i18nData[colName][locale], i18nData[colName], i18nData)
                    if (i18nData[colName][locale]) {
                        this._logger.debug('            Found....', i18nRec)
                        i18nRec[colName] = i18nData[colName][locale];                    
                    }
                }

                // create i18n record
                this._logger.debug('            About to save i18n record...', i18nRec)
                if ( !_.isEmpty(i18nRec) ) {

                    i18nRec.locale = locale;
                    i18nRec['ownerId'] = instance.id;

                    this._logger.debug('                About to save i18n record...', i18nRec)

                    createOrSaveOptions.uniqueKeys.locale = locale;
                    let dao = this._container.getBean<TypeormDao<any>>('i18n' + this.capitalize(setName) + "Dao");
                    await dao.save(handler, i18nRec, createOrSaveOptions);

                }
            }        
        }

    }

    private async createOrSaveRec(handler: YinzConnection | YinzTransaction, setName: string, keys: string[], rec: any, i18nData: any, options?: DataLoaderFunctionsOptions): Promise<any> {

        this._logger.trace(`Enter Dataloader.createOrSaveRec(handler %s, setName %s,  keys %j, rec %j, i18nData %j, options %s)`, handler.id, setName, keys, rec, i18nData, options);

        options = options || {};

        let dfltLang = options.defaultLang || 'en';

        // variable that will hold the actual record to create
        let clone = {};

        let createOrSaveOptions: any = {...options};

        // populate unique keys 
        if ( keys ) {
            createOrSaveOptions.uniqueKeys = {};
            for ( let key of keys ) {
                createOrSaveOptions.uniqueKeys[key] = rec[key];
            }
        }
        
        for ( let colName of Object.keys(rec) ) {
            

            // if rec is an array
            if ( Array.isArray(rec[colName])) {

                // the value of the column is an array too
                clone[colName] = new Array(rec[colName].length)

                // ask to update relationship
                createOrSaveOptions.include = createOrSaveOptions.include || [];
                createOrSaveOptions.include = _.isArray(createOrSaveOptions.include) ? createOrSaveOptions.include : [createOrSaveOptions.include];
                createOrSaveOptions.include.push(colName);                
                
                // get each reference in the array, lookup its underlying object then add it to the array

                // we're sure rec[colName] is an array here

                // rec[colName].forEach( async (element, index) => {
                
                for ( let index = 0; index < rec[colName].length; index++) {

                    let element = rec[colName][index];

                    let object = await this.lookupRefObject(handler, setName, colName, element.$ref, createOrSaveOptions)
                    clone[colName][index] = object;

                    // append properties other than '$ref' to the found object
                    for (let propName in element) {
                        if (propName !== '$ref' && element.hasOwnProperty(propName)) {
                            clone[colName][index][propName] = element[propName];
                        }
                    }
                } 
            }
            // if rec is a $ref
            else if ( _.isObject(rec[colName] && rec[colName].$ref)) {

                createOrSaveOptions.include = createOrSaveOptions.include || [];
                createOrSaveOptions.include = _.isArray(createOrSaveOptions.include) ? createOrSaveOptions.include : [createOrSaveOptions.include];
                createOrSaveOptions.include.push(colName);  

                let object = await this.lookupRefObject(handler, setName, colName, rec[colName].$ref, createOrSaveOptions)
                clone[colName] = object;
            }
            // if rec is a $i18n
            else if (_.isObject(rec[colName] && rec[colName].$i18n)) {
                i18nData[colName] = rec[colName].$i18n;

                clone[colName] = rec[colName].$i18n.$dflt || rec[colName].$i18n[dfltLang];                
            }
            else {                
                clone[colName] = rec[colName];
            }
           
        }

        // create the clone

        let dao = this._container.getBean<TypeormDao<any>>(setName + 'Dao');
                
        if (createOrSaveOptions.uniqueKeys) {
            return  await dao.save(handler, clone, createOrSaveOptions);
        } else {
            return  await dao.create(handler, clone, createOrSaveOptions);
        }                
    }

    private async lookupRefObject(handler: YinzConnection | YinzTransaction, setName: string, refName: string, filter: any, options?: DataLoaderFunctionsOptions): Promise<any> {

        this._logger.trace(`Enter Dataloader.lookupRefObject(handler %s, setName %s,  refName %s, filter %j, options %j)`, handler.id, setName, refName, filter, options);
                        
        let setModelClass = this._cache[setName] ? this._cache[setName] : this._container.getClazz(this.capitalize(setName));
                
        // make sure that $ref is an association in set
        this._logger.debug(`                setModelClass`, refName);
        let repo = handler.native.getRepository(setModelClass);

        if  ( !(Object.keys(repo.metadata.propertiesMap).includes(refName)) ) {
            throw new Exception('ERR_TOOLS_DATALOADER__DATALOADER__LOOKUP_REF_OBJ__UNEXP_REF', {
                message: 'The property ' + refName + ' does not belong to the set ' + setName + '!',
                setName,
                refName
            });
        }                                                    
        
        let daoPrefix;
        
        // get daoPrefix in the case of ManyToMany, OneToOne, ManyToOne and OneToMany
        for ( let relation of repo.metadata.relations ) {

            if ( relation.propertyPath === refName) {
                // @ts-ignore // we know relation.type is a class, typescript doensn't allow us to access the name field by defaults
                daoPrefix = _.camelCase(relation.type.name);                
                break;
            }            
        }                    

        let cacheId = daoPrefix + "-" + JSON.stringify(filter)

        this._logger.info(``, filter, options)
        let findOptions = {...options, include: []};
        let result = this._cache[cacheId] ? this._cache[cacheId] : await this._container.getBean<TypeormDao<any>>(daoPrefix + "Dao").findSingleByFilter(handler, filter, findOptions);

        this._cache[cacheId] = result;
        
        this._logger.debug(`retsult --> `, result)
        return result;        
    }

  
    private capitalize (s) {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

}