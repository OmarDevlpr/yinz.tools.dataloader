import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import DataLoader from "./DataLoader";



const registerBeans = (container: Container) => {

    const dataLoader = new DataLoader({
        container,
        logger: container.getBean<Logger>('logger')
    })
    
    container.setBean('dataLoader', dataLoader);    

};

const registerClazzes = (container: Container) => {

    container.setClazz('DataLoader', DataLoader);
   
};


export {
    registerBeans,
    registerClazzes
};