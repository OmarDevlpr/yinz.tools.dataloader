import { Model, TypeormDao } from "@yinz/commons.data";
import { Entity, Column, JoinColumn, OneToOne, OneToMany, ManyToOne, JoinTable, ManyToMany } from "typeorm";
import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import AccessManager from "@yinz/access.data/ts/AccessManager";

@Entity()
export class TestPerson extends Model {

    @Column({ unique: true})
    code: string

    @Column({nullable: true})
    name: string

    @OneToOne(type => Contact)
    @JoinColumn()
    contact: Contact;

    @OneToMany(type => Account, accounts => accounts.user)
    accounts: Account[];

    @ManyToMany(type => Position)
    @JoinTable()
    positions: Position[]

    @ManyToOne(type => Company, company => company.testPersons)
    company: Company;
}

@Entity()
export class Contact extends Model {

    @Column({ unique: true })
    email: string

    @Column({ unique: true })
    phone: string

}


@Entity()
export class Account extends Model {

    @Column({ unique: true })
    displayName: string    

    @ManyToOne(type => TestPerson, testPerson => testPerson.accounts)
    user: TestPerson;

}

@Entity()
export class Position extends Model {

    @Column()
    name: string;

    @OneToMany(type => PositionType, posType => posType.position)    
    positionTypes: PositionType[]

}

@Entity()
export class PositionType extends Model {
    
    @Column()
    name: string;

    @ManyToOne(type => Position)
    position: Position;
}

@Entity()
export class Company extends Model {

    @Column({unique: true})
    code: string;

    @Column()
    name: string;

    @OneToMany(type => TestPerson, testPerson => testPerson.company)
    testPersons: TestPerson[];

}

@Entity()
export class I18nCompany extends Model {

    @Column()
    locale: string;
    
    @Column({})
    ownerId: number;

    @Column()
    name: string;    

}

@Entity()
export class I18nTestPerson extends Model {

    @Column()
    locale: string;

    @Column({})
    ownerId: number;

    @Column()
    name: string;

}



export const registerDaos = (container: Container) => {

    let testPersonDao = new TypeormDao <TestPerson>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: TestPerson        
    })

    let accountDao = new TypeormDao<Account>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: Account
    })

    let companyDao = new TypeormDao<Company>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: Company
    })

    let contactDao = new TypeormDao<Contact>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: Contact
    })

    let positionDao = new TypeormDao<Position>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: Position
    })

    let positionTypeDao = new TypeormDao<PositionType>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: PositionType
    })


    let i18nCompanyDao = new TypeormDao<I18nCompany>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: I18nCompany
    })

    let i18nTestPersonDao = new TypeormDao<I18nTestPerson>({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        model: I18nTestPerson
    })
    
    container.setClazz('TestPerson', TestPerson)
    container.setClazz('Account', Account)
    container.setClazz('Company', Company)
    container.setClazz('Contact', Contact)
    container.setClazz('Position', Position)
    container.setClazz('PositionType', PositionType)
    container.setClazz('I18nCompany', I18nCompany)
    container.setClazz('I18nTestPerson', I18nTestPerson)

    container.setBean('testPersonDao', testPersonDao)
    container.setBean('accountDao', accountDao)
    container.setBean('companyDao', companyDao)
    container.setBean('contactDao', contactDao)
    container.setBean('positionDao', positionDao)
    container.setBean('positionTypeDao', positionTypeDao)
    container.setBean('i18nCompanyDao', i18nCompanyDao)
    container.setBean('i18nTestPersonDao', i18nTestPersonDao)

    return {
        testPersonDao,
        accountDao,
        companyDao,
        contactDao,
        positionDao,
        i18nCompanyDao,
        positionTypeDao
    }

};