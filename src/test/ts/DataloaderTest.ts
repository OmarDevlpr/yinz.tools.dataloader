// libs
import * as assert from "assert";
// import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDataSource, TypeormDao, Model } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import DataLoader from "../../main/ts/DataLoader";
import AccessManager from "@yinz/access.data/ts/AccessManager";
import { Entity, Column } from "typeorm";
import { Logger } from "@yinz/commons";
import { registerDaos, TestPerson, Position, PositionType } from "./TestPerson";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',        
        '@yinz/access.data/ts',        
        process.cwd() + '/dist/main/ts'
    ],
});

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');
const dataLoader = container.getBean<DataLoader>('dataLoader');

let conn: YinzConnection;
let trans: YinzTransaction;

@Entity()
class I18nUser extends Model {

    @Column()
    locale: string;

    @Column()
    code: string;

    @Column({})
    ownerId: number;
}

let i18nUserDao = new TypeormDao({
    logger: container.getBean<Logger>('logger'),
    accessManager: container.getBean<AccessManager>('accessManager'),
    model: I18nUser
})

container.setClazz('I18nUser', I18nUser)
container.setBean('i18nUserDao', i18nUserDao)

const { testPersonDao, accountDao, companyDao, contactDao, positionDao, positionTypeDao } = registerDaos(container);

describe('| tools.dataloader<Dataloader.load>', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('\n' +
        '       | Conditions: \n' +
        '       | - options not supplied \n' +        
        '       | Expected :  \n' +
        '       | -> load data.  \n' +
        '', async () => {
        
            // 1. prepare test-data
            let filenames = [
                // './src/test/data/data-only.yml',
                './src/test/data/$files-as-array.yml',
                // './src/test/data/$files-as-string.yml'            
            ]

            try {

                // 2. execute test
                let result = await dataLoader.load(trans, filenames, { user: "__super_user__" });

                // 3.1 assert result 

                assert.ok(result)

                let companyResult = await companyDao.findAll(trans, {user: "__super_user__"});
                let positioResult = await positionDao.findAll(trans, {user: "__super_user__"});                
                let contactResult = await contactDao.findAll(trans, {user: "__super_user__"});                
                let accountResult = await accountDao.findAll(trans, {user: "__super_user__"});                
                let testPersonResult = await testPersonDao.findAll(trans, {user: "__super_user__", include: ['company', 'positions', 'contact', 'accounts'], locale: 'fr' });

                // 3.2 assert company
                assert.ok(companyResult)
                assert.strictEqual(companyResult.length, 1)
                assert.strictEqual(companyResult[0].code, "THE_ONLY_EXISTING_COMPANY")
                assert.strictEqual(companyResult[0].name, "The only existing company")

                // 3.3 assert position
                assert.ok(positioResult)
                assert.strictEqual(positioResult.length, 4)            
                assert.strictEqual(positioResult[0].name, "manager")
                assert.strictEqual(positioResult[1].name, "staff")
                assert.strictEqual(positioResult[2].name, "cto")
                assert.strictEqual(positioResult[3].name, "ceo")

                // 3.4 assert contanct
                assert.ok(contactResult)
                assert.strictEqual(contactResult.length, 2)
                assert.strictEqual(contactResult[0].phone, "95451892")
                assert.strictEqual(contactResult[1].phone, "95451894")
                assert.strictEqual(contactResult[0].email, "first@email.com")
                assert.strictEqual(contactResult[1].email, "second@email.com")
                
                // 3.4 account assert
                assert.ok(accountResult)
                assert.strictEqual(accountResult.length, 2)                
                assert.strictEqual(accountResult[0].displayName, "firstAccount")
                assert.strictEqual(accountResult[1].displayName, "secondAccount")

                // 3.5 assert testPerson
                assert.ok(testPersonResult)
                assert.strictEqual(testPersonResult.length, 1)
                assert.strictEqual(testPersonResult[0].code, "FIRST_TEST_PERSON")
                assert.strictEqual(testPersonResult[0].name, "premier test personne")
                assert.strictEqual(testPersonResult[0].contact.email, "first@email.com")
                assert.strictEqual(testPersonResult[0].contact.phone, "95451892")
                assert.strictEqual(testPersonResult[0].accounts.length, 2)
                assert.ok(testPersonResult[0].accounts[0].displayName === "firstAccount" || "secondAccount")
                assert.ok(testPersonResult[0].accounts[1].displayName === "firstAccount" || "secondAccount")
                assert.strictEqual(testPersonResult[0].positions.length, 2)
                assert.ok(testPersonResult[0].positions[0].name === "ceo" || "cto")
                assert.ok(testPersonResult[0].positions[1].name === "ceo" || "cto")
                assert.strictEqual(testPersonResult[0].company.code, "THE_ONLY_EXISTING_COMPANY")
                assert.strictEqual(testPersonResult[0].company.name, "The only existing company")
                
                // 3. compare outcome
                // // data-only file container 2 profiles records and one user record

                // // 3.1 check result loaded records
                // assert.ok(result)

                // assert.ok(result.profile)                
                // assert.ok(result.profile.recs)                
                // assert.strictEqual(result.profile.recs.total, 2)                
                // assert.strictEqual(result.profile.recs.loaded, 2)                
                // assert.strictEqual(result.profile.recs.skipped, 0)                

                // assert.ok(result.user)
                // assert.ok(result.user.recs)
                // assert.strictEqual(result.user.recs.total, 1)
                // assert.strictEqual(result.user.recs.loaded, 1)
                // assert.strictEqual(result.user.recs.skipped, 0)                

                // // 3.2 check that profile was created
                // let profileResult = await profileDao.findAll(trans, {user: "__super_user__"});
                // assert.ok(profileResult)
                // assert.strictEqual(profileResult.length, 2)
                // assert.ok(profileResult[0].code === "PROFILE_ONE" || "PROFILE_TWO")
                // assert.ok(profileResult[0].name === "profile one" || "profile two")
                // assert.ok(profileResult[1].code === "PROFILE_ONE" || "PROFILE_TWO")
                // assert.ok(profileResult[1].name === "profile one" || "profile two")

                // // 3.3 check that user was created
                // let userResult = await userDao.findAll(trans, {user: "__super_user__", include: ['profiles']});

                // assert.ok(userResult)
                // assert.strictEqual(userResult.length, 1)
                // assert.strictEqual(userResult[0].code, 'code1')
                // assert.strictEqual(userResult[0].status, 'A')

                // assert.ok(userResult[0].profiles)
                // assert.strictEqual(userResult[0].profiles.length, 2)
                // assert.ok(userResult[0].profiles[0].code === "PROFILE_ONE" || "PROFILE_TWO")
                // assert.ok(userResult[0].profiles[0].name === "profile one" || "profile two")
                // assert.ok(userResult[0].profiles[1].code === "PROFILE_ONE" || "PROFILE_TWO")
                // assert.ok(userResult[0].profiles[1].name === "profile one" || "profile two")
                    
                // // 3.4 check i18n data
                // let i18nUser = await i18nUserDao.findAll(trans, {user: "__super_user__"});
                // console.log(i18nUser)

                // let userResultAr = await userDao.findAll(trans, { user: "__super_user__", include: ['profiles'], locale: 'ar' });
                // let userResultFr = await userDao.findAll(trans, { user: "__super_user__", include: ['profiles'], locale: 'fr' });

                // assert.ok(userResultAr)
                // assert.strictEqual(userResultAr.length, 1)
                // assert.strictEqual(userResultAr[0].code, 'kode1')
                // assert.strictEqual(userResultFr[0].code, 'coude1')
                // assert.strictEqual(userResultAr[0].status, 'A')

                // assert.ok(userResultAr[0].profiles)
                // assert.strictEqual(userResultAr[0].profiles.length, 2)
                // assert.ok(userResultAr[0].profiles[0].code === "PROFILE_ONE" || "PROFILE_TWO")
                // assert.ok(userResultAr[0].profiles[0].name === "profile one" || "profile two")
                // assert.ok(userResultAr[0].profiles[1].code === "PROFILE_ONE" || "PROFILE_TWO")
                // assert.ok(userResultAr[0].profiles[1].name === "profile one" || "profile two")



            } catch ( e ) {

                console.error(e)
                throw e;

            }                        
    });

    it.only('-> eager load', async () => {

        let options = { user: "__super_user__" };

        let positionType = new PositionType();
        positionType.name = "positionTypeName";
        positionType = await positionTypeDao.create(trans, positionType, options);


        let position = new Position();
        position.name = "positionName"
        position.positionTypes = [positionType]
        position = await positionDao.create(trans, position, { ...options, include: ['positionTypes']});


        let testPersons: TestPerson[] = new Array(5).fill(0).map(x => ({ code: "" + Math.random(),  name: "chance.string({ length: 10 })", positions: [position] } as TestPerson));
        await testPersonDao.createAll(trans, testPersons, {...options, include: ['positions']});


        let res = await testPersonDao.findAll(trans, { ...options, include: ['positions', 'positions.positionTypes' ] })
        let posRes = await positionDao.findAll(trans, { ...options, include: ['positionTypes']})
        let postypRes = await positionTypeDao.findAll(trans, { ...options  })

        console.log(JSON.stringify(res[0]))
        console.log(JSON.stringify(posRes[0]))
        console.log(JSON.stringify(postypRes[0]))
    })


});
